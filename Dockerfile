FROM node:latest as builder

WORKDIR /app
COPY package.json ./package.json
RUN npm i @angular/cli -g
COPY . /app

RUN rm -rf node_modules

RUN npm install
RUN npm run webpack:build:prod

FROM nginx:latest
RUN  rm -rf /usr/share/nginx/html/*
COPY --from=builder  /app/dist /usr/share/nginx/html/
COPY nginx.conf /etc/nginx/conf.d/default.conf
