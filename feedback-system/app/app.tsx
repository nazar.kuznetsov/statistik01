import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import MainContainer from '../modules/main-container';
import {Albums} from '../modules/albums';
import Login from '../modules/login';
import NotFound from '../modules/not-found';

class App extends Component {
    render() {
        return (
            <Switch>
                <Route path="/login" component={Login}/>
                <Route path="/saga" component={Albums}/>
                <Route path="/" component={MainContainer}/>
                <Route component={NotFound} />
            </Switch>
        );
    }
}

export default App;
