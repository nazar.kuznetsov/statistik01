import {CONTENT_TYPE_JSON} from '../constants';

const isJson = (response: any) => response.headers.get('Content-Type') === CONTENT_TYPE_JSON;

export const responseToPromise = (r: Response): Promise<object | string> => (isJson(r) ? r.json() : r.text());
