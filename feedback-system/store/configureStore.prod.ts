import {createStore, applyMiddleware, compose} from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from 'feedback-system/saga/root';
import rootReducer from '../reducers';
// import apiMiddleware from 'feedback-system/middleware/api'

import {IState} from './typedef';

const sagaMiddleware = createSagaMiddleware();

export default function configureStore(preloadedState: IState) {
    const store = createStore(
        rootReducer,
        preloadedState,
        compose(applyMiddleware(
            sagaMiddleware
            // apiMiddleware
        ))
    );

    sagaMiddleware.run(rootSaga);

    return store;
}
