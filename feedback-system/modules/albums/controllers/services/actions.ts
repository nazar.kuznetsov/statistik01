import {ApiMethods} from 'feedback-system/middleware';
import {MOCKED_ACTION_REQUEST, MOCKED_FETCH_ACTION} from './constants';

export const mockedFetchAction = () => ({
    CALL_API: {
        endpoint: 'https://jsonplaceholder.typicode.com/todos/1',
        method: ApiMethods.GET,
        types: MOCKED_FETCH_ACTION
    }
});

export const mockedAction = (value: string) => ({
    type: MOCKED_ACTION_REQUEST,
    value
});
