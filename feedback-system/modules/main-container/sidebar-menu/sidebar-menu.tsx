import React from 'react';
import {Link} from 'react-router-dom';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {withStyles} from '@material-ui/core/styles';

const styles = (theme: any) => ({
    root: {
        width: '100%',
        maxWidth: '360px',
        backgroundColor: theme.palette.background.paper
    }
});

const QualityTeamFeedbackLink = (props: any) => <Link to="/" {...props} />;
const FeedbackConsolidationLink = (props: any) => <Link to="/feedback-consolidation" {...props} />;
const StatisticLink = (props: any) => <Link to="/statistic" {...props} />;
const SidebarMenu = (props: any) => {
    const {classes} = props;

    return (
        <>
            <List component="nav" className={classes.root}>
                <ListItem button={true} component={QualityTeamFeedbackLink}>
                    <ListItemText primary="Quality Team Feedback"/>
                </ListItem>
                <Divider/>
                <ListItem button={true} divider={true} component={FeedbackConsolidationLink}>
                    <ListItemText primary="Feedback consolidation"/>
                </ListItem>
                <ListItem button={true} component={StatisticLink}>
                    <ListItemText primary="Statistic"/>
                </ListItem>
            </List>
        </>
    );
};

export default withStyles(styles)(SidebarMenu);
