import React from 'react';
import { TableCell, TableSortLabel } from '@material-ui/core';

interface ITitle {
    label: string;
    orderBy: string;
    derection: string;
    handleClick: (id: string) => {};
    id: string;
}

const Title = ({ label, handleClick, id, orderBy, derection }: ITitle) => {
    const click = () => {
        handleClick(id);
    };

    return (
        <TableCell
            onClick={click}>{label}
            <TableSortLabel
                active={orderBy === id}
                direction={derection}
            />
        </TableCell>
    );
};

export default Title;
