import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Feedback from './feedback';
import Title from './title';

interface IFeedback {
  nameTo: string;
  nameFrom: string;
  rating: number;
  comments: string;
  id: string;
}

interface ITheme {
  root: {
    width: string,
    marginTop: number,
    overflowX: string
  };
  table: {
    minWidth: number
  };
  spacing: {
    unit: number;
  };
}

/* Table title
================================================================================= */
const title = [
  { id: 'nameTo', label: 'От кого', numeric: false },
  { id: 'nameFrom', label: 'Кому', numeric: false },
  { id: 'rating', label: 'Оценка', numeric: false },
  { id: 'comments', label: 'Коментарий', numeric: false }
];

/* Data
================================================================================= */
const data: IFeedback[] = [
  {
    nameTo: 'Андрей  Кличко',
    id: '001',
    nameFrom: 'Тлекс Дрон',
    rating: 3,
    comments: 'Ок'
  },
  {
    nameTo: 'Петро  Порошенко',
    id: '002',
    rating: 2,
    nameFrom: 'Макс Опг',
    comments: 'Молодец'
  },
  {
    nameTo: 'Виктор  Янукович',
    id: '003',
    nameFrom: 'Алкоголик Черный',
    rating: 1,
    comments: 'Плохо'
  },
  {
    nameTo: 'Аладимир Путин',
    id: '004',
    nameFrom: 'Янукович Виктор',
    rating: 4,
    comments: 'Молодец'
  },
  {
    nameTo: 'Барак Обама',
    id: '005',
    rating: 8,
    nameFrom: 'Космос Олеп',
    comments: 'Класс'
  },
  {
    nameTo: 'Ким Чен Ир',
    id: '006',
    rating: 7,
    nameFrom: 'Владимир Кличко',
    comments: 'Круто'
  }
];

const styles = (theme: ITheme) => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto'
  },
  table: {
    minWidth: 700
  }
});

class SimpleTable extends Component {
  state = {
    data,
    orderBy: 'nameTo',
    direction: 'desc'
  };

  sorting = (type: string) => {
    const clone = [...this.state.data];
    const types = this.state.orderBy;
    let sort;
    if (this.state.direction === 'desc') {
      sort = type === 'string'
        ? (a, b) => a[types].localeCompare(b[types])
        : (a: object, b: object) => a[types] - b[types];

    } else {
      sort = type === 'string'
        ? (a, b) => b[types].localeCompare(a[types])
        : (a: object, b: object) => b[types] - a[types];
    }

    const feedback = clone.sort(sort);
    this.setState({ data: feedback });

  }

  derection = (name: string, type: 'string') => {
    this.setState((state) => {
      return {
        [name]: !state[name],
        orderBy: name
      };
    }, () => {
      this.sorting(type);
    });
  }

  /* Sorting On Click Title
  ================================================================================= */
  handleClick = (id: string) => {
    const type = id === 'rating' ? 'number' : 'string';
    this.setState((state) => {
      let result = '';
      if (state.orderBy === id) {
        result = state.direction === 'desc' ? 'asc' : 'desc';
      } else {
        result = 'desc';
      }
      return {
        direction: result
      };
    }, () => this.derection(id, type));
  }

  /* Delete feedback
  ================================================================================= */
  delete = (id: string) => {
    const feedback = [...this.state.data].filter(element => element.id !== id);
    this.setState({data: feedback});
  }

  componentDidMount() {
    this.sorting('string');
  }

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        {
          this.state.data.length > 0
            ? <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  {
                    title.map(headline =>
                      <Title
                        key={headline.id}
                        label={headline.label}
                        id={headline.id}
                        derection={this.state.direction}
                        orderBy={this.state.orderBy}
                        handleClick={this.handleClick}
                      />
                    )}
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.data.map(row => (
                  <TableRow key={row.id}>
                    <Feedback
                      nameTo={row.nameTo}
                      nameFrom={row.nameFrom}
                      rating={row.rating}
                      id={row.id}
                      comments={row.comments}
                      onDelete={this.delete}
                    />
                  </TableRow>
                ))
                }
              </TableBody>
            </Table>
            : null
        }
      </Paper>
    );
  }
}

export default withStyles(styles)(SimpleTable);
